package main

import (
	"fmt"
	"runtime"
	"math"
)

func add(x byte, y byte) (int, int) {
	var z = int(x + y) // casting is necessary because input values are bytes, not ints :-/
	return z, 7
}

func main() {
	
	// Play with returning multiple values of different types.
	var z1, z2 = add(2, 200)
	
	z1 += z2 // if z2 isn't used, Go complains "z2 declared but not used"
	
	fmt.Println("My favorite # is", z1)
	
	var os = runtime.GOOS
	
	// Switch versus if/else, IMO case/switch is a waste of time in every language.
	switch os {
		case "linux":
			fmt.Println("Linux")
		default:
			fmt.Println("Unknown OS")
	}
	
	// Easier to read, why bother with "switch" ?
	if os == "linux" {
		fmt.Println("Linux")
	} else {
		fmt.Println("Unknown OS")
	}
	
	type Name struct {
		first string
		last string
	}
	
	var pete = Name{"PJ", "Brunet"}
	fmt.Println(pete.first, pete.last)
	
	var joe = new(Name)
	joe.first = "Joe"
	fmt.Println("New Joe:", joe.first)
	
	var myArray = [3]int{1, 2, 3}
	myArray[0] = 0;
	fmt.Println(myArray)
	
	var namesArr [2]Name // array of structs (two Names)
	
	namesArr[0].first = "Joe"
	namesArr[0].last = "Blow"
	
	namesArr[1] = Name{"PJ", "Brunet"} // faster way to define a Name
	
	fmt.Println(namesArr)
	
	// We imported "math" to calculate MaxInt
	var maxint int // Just to see if "int" is indeed 64bit.
	maxint = math.MaxInt64
	fmt.Println("MaxInt:", maxint) // Yep, it is :-D
	
	// I assume if Names has sub structs, this essentially adds more dimensions to the array.
	// https://www.golangprograms.com/go-language/struct.html
	// Note: Go doesn't complain if a struct is defined but not used.
	type User struct {
		name Name
		country string // What's the maximum string length? Apparently it's limited by RAM available https://stackoverflow.com/a/66810396/722796
		city string // We don't really need a 3d dimension for this, it can go in the 2nd dimension.
	}
}
